---
Title: Music report
Date: 2020-7-27
Category: GitLab
Tags: pelican, gitlab
---

# 東南亞傳統樂器

## 越南

越南的音樂以合奏音樂為主，不論是宮廷的合奏樂和戲劇的合奏音樂
，大都模仿自中國，樂器的合奏有所謂的「五絕」，即五種樂器：
箏、胡琴、月琴、琵琶、三弦的合奏音樂。

### 獨弦琴

 <img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/Myanmar1.jpg" width = "300" height = "200" alt="獨弦琴"  />

越南的獨弦琴與雲南少數民族京族的樂器極為類似，尤其抑揚頓挫的
滑音極有特色。
即興演奏是極為平常的表演方式，無論在曲子一開始的序奏，甚至全
曲都是即興演奏，經常有極多的裝飾音。

### 越南箏

<img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/Myanmar2.jpg" width = "300" height = "200" alt="越南箏" />

又名彈箏，形制與中國十六弦箏極為相似，也是由十六根弦所組成的，箏體為木製，弦為鋼弦，弦柱為倒V形，支撐每根弦。
古代的彈箏為16弦，故有一個別名是「彈十六」，在1950年代時，由阮永寶將其改為十七弦，使得彈箏在越南逐漸普及。
彈箏源自於中國的古箏，與日本的日本箏和朝鮮的伽倻琴都有關聯。與古箏的主要區別在於其有2排弦柱，而古箏只有1排弦柱。

## 寮國

### 寮國笙

<img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/laos1.jpg" width ="200" height = "300" alt="寮國笙" />

寮國笙稱為khene（或khaen），是一種竹製的笙簧樂器，將銅簧或銀簧鑲入竹管壁當中，再將一塊木箱將竹管包起來，
當作風箱稱為「匏」（dao），khene在寮國可以用來獨奏，也可用來合奏或為歌者伴奏，通常演奏者一定是男性，
比較特別的是，寮笙共有12/14/16/18管等四種大小樣式。

## 緬甸

### 鳳首箜篌

<img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/vietnam1.jpg" width ="300" height ="200" alt="鳳首箜篌" />

鳳首箜篌於緬甸被尊稱為樂器之王，其名稱是源於阿拉伯與波斯，經過了印度，傳到緬甸，並於此改變了其形制。
它是以五聲音階為模式來調音，但卻可奏出自然音階。

### 緬甸木琴

<img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/vietnam2.jpg" width ="300" height = "200" alt="緬甸木琴" />

最早提及木琴的記載，可見於1479年的卡亞尼（Kalyani inscription）石碑文上。泰國木琴源於緬甸木琴，有證據顯示木琴出現於緬甸的時間早於泰國。
現代的緬甸木琴是由二十四個竹製琴鍵懸吊於船形共鳴箱之上而構成，由包裹上軟質布料的琴槌來演奏。

## 印尼

### 甘美朗

<img src="https://gitlab.com/Jimmy1307/jimmy1307.gitlab.io/-/raw/master/content/posts/indonesia1.jpg" width="300" height="200" alt="甘美朗" />

印尼傳統音樂種類眾多，其中以鑼鼓合奏的「甘美朗」（gamelan）最著名，主要以打擊樂器，大多用於傳統藝能與儀式慶典的大型合奏，屬於印尼最代表性的樂種。
源自於宮廷，後來廣泛分布於民間，使用的樂器大多為銅製樂器，少數為木製、竹製。